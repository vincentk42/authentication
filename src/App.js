import React, { Component } from 'react';
import { View, Text} from 'react-native';
import firebase from 'firebase';
import { Header, Button, CardSection, Spinner } from './components/common';
import LoginForm from './components/LoginForm';

class App extends Component {
    // constructor() {
    //     super()
    //     this.state={
    //         loggedIn:null
    //     }
    // }

    state = {loggedIn: null}

    componentWillMount() {
        firebase.initializeApp({
            apiKey: 'AIzaSyDVKcvafd8M8ZKubUZTIOXEByLlUXyvfAY',
            authDomain: 'authentication-644da.firebaseapp.com',
            databaseURL: 'https://authentication-644da.firebaseio.com',
            projectId: 'authentication-644da',
            storageBucket: 'authentication-644da.appspot.com',
            messagingSenderId: '260634425054'
          }); 
        firebase.auth().onAuthStateChanged((user) => {/*NOTE: onAuthoStateChanged covers both logging in and logging out*/
            if(user) {
                this.setState({
                    loggedIn:true,
                })
            } else {
                this.setState({
                    loggedIn: false,
                })
            }
        });
    }

    renderContent() {
        switch(this.state.loggedIn) {
            case true:
                return (
                    <CardSection>
                    <Button onPress={()=>firebase.auth().signOut()}>
                        log out
                    </Button>
                    </CardSection>
                );
            case false:
                return <LoginForm />;
            default:
                return <View style={{height:100}}><Spinner style={{marginTop:50}} size="large" /></View>;
        }
    }
    render(){
        return(
            <View>
                <Header headerText="Authentication"/>
                {this.renderContent()}
            </View>

        );
    }
}

export default App;