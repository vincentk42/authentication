import React, {Component} from 'react';
import { Text } from 'react-native';
import firebase from 'firebase'
import {Button, Card, CardSection, Input, Spinner } from './common';
 


class LoginForm extends Component {
    // constructor(){
    //     super()
    //     this.state={
    //         email:'',
    //         password:'',
    //         error:'',
    //         loading:'',
    //     }
    // }
    state = {
            email: ''
            , password: ''
            , error: ''
            , loading: false
            ,
        };
   
    onButtonPress() {
        const {email, password} = this.state;

        
        firebase.auth().signInWithEmailAndPassword(email, password)
        
        .then(this.onLoginSuccess.bind(this))
        .catch(() => {
          firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(this.onLoginSuccess.bind(this))
            .catch(this.onLoginFail.bind(this));
        });

        this.setState({
            error:''
            , loading: 'true'
            
        })
    }

    onLoginFail() {
        this.setState({
            error:"authentication failed",
            loading: false
        });
    }

    onLoginSuccess() {
        console.log('this is working')
        this.setState({
            email:''
            , password: ''
            , loading: false
            , error: ''
            ,
        })
    }

    renderButton() {
        if(this.state.loading) {
            return(<Spinner size="small"/>)
        }
        return (
            <Button onPress={this.onButtonPress.bind(this)} 
            >Log in
            </Button>
        );
    }

    render(){
        return(
            <Card>
                <CardSection >
                    <Input
                        label="email"
                        value={this.state.email}
                        onChangeText={email => this.setState({email})}
                        placeholder="user@gmail.com"
                        
                    />
                </CardSection>
                
                
                <CardSection>
                    <Input
                    /*note:  because we're passing a boolean that's true, we do not need to add it*/
                        secureTextEntry
                        placeholder="password"
                        label="password"
                        value={this.state.password}
                        onChangeText={password => this.setState({password})}
                    />

                </CardSection>
                <Text style={styles.errorTextStyle}>{this.state.error}</Text>
                <CardSection>
                    {this.renderButton()}
                </CardSection>
            </Card>
        )
    }
}


const styles = {
    errorTextStyle:{
        fontSize: 20,
        alignSelf:'center',
        color: 'red',
    }
};

export default LoginForm;